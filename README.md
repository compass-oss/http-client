# Http REST Client

Wraps RestTemplate communication with builders for easier usage and adds options for repeating requests.

The basic concept behind this library is setting expected status, retries and their timeouts. If response is returned
with different status than was excepted:
1. If status is marked as `doNotRepeatFor` a `ResponseException` is thrown and no more calls are tried.
2. If response status indicates that repeated call may succeed (eg. `502`) a next attempt is made if not exceeding max attempts

Re-tries are not fired right away but after defined `timeout`.


## Usage
This library contains AutoConfiguration with `RestTemplate`, `ObjectMapper` beans and can be enabled
by using `@EnableDiscoveryClient`.

The configuration is active for profiles != `tests` profile. 

Application using this library has to have

* `spring-boot-starter-web:3.0.5`
* `spring-boot-starter-security:3.0.5`
* `spring-cloud-starter-netflix-eureka-client:2022.0.1`

on classpath.


### Examples

```java

@Component
@Scope("prototype")
class ExampleRestClient extends HttpRestClient {

    private static final Logger logger = getLogger(ExampleRequestRestClient.class);
    private final ObjectMapper objectMapper;
    
    ExampleRestClient(RestTemplate restTemplate, ObjectMapper objectMapper) {
        super(restTemplate);
        this.objectMapper = objectMapper;
    }

    /**
     * Sends GET request to retrieve an Example
     *
     * @param name Name of the example we want to retrieve
     * @return Example with given name, if exists. Or an empty optional if does not or an error is returned from EXAMPLE-SERVICE
     */
    Optional<Example> getExampleData(String name) {
        try {
            ExportResponse response = send(
                    get(URI.create("EXAMPLE-SERVICE/examples/" + name), Example.class, restTemplate)
                            .header(HttpHeaders.AUTHORIZATION, "Basic XYZ")
                            .doNotRepeatFor(HttpStatus.NOT_FOUND)
                            .tries(3)
                            .json()
                            .timeout(60_000L), Set.of(HttpStatus.OK, HttpStatus.ACCEPTED));

            return Optional.of(response);
        } catch (ResponseException | JsonProcessingException exception) {
            logger.error("An error occurred while retrieving an Example with name = {}", name, exception);
            return Optional.empty();
        }
    }

    /**
     * Creates a new example in EXAMPLE-SERVICE
     * 
     * @param example An example we want to create
     * @throws RuntimeException When an example could not be created in EXAMPLE-SERVICE
     */
    void createExample(Example example) {
        try {
            send(
                    post(URI.create("EXAMPLE-SERVICE/examples"), Void.class, restTemplate)
                            .body(objectMapper.writeValueAsString(example))
                            .header(HttpHeaders.AUTHORIZATION, "Basic XYZ")
                            .doNotRepeatFor(HttpStatus.UNPROCESSABLE_ENTITY)
                            .doNotRepeatFor(HttpStatus.BAD_REQUEST)
                            .tries(3)
                            .json()
                            .timeout(60_000L), Set.of(HttpStatus.OK, HttpStatus.ACCEPTED));
        } catch (ResponseException | JsonProcessingException exception) {
            logger.error("An error occurred while creating an Example = {}", example);
            throw new RuntimeException(exception);
        }
    }
    
}
```