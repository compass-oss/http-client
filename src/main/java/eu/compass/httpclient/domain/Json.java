package eu.compass.httpclient.domain;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class Json {

    protected static final Logger LOG = LoggerFactory.getLogger(Json.class);

    public String json() {
        try {
            return new ObjectMapper()
                    .setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY)
                    .writeValueAsString(this);
        } catch (JsonProcessingException e) {
            LOG.error("Unable to convert {} to JSON. Returning null", this, e);
            return null;
        }
    }
}
