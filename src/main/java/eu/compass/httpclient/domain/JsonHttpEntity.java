package eu.compass.httpclient.domain;

import org.springframework.http.HttpEntity;
import org.springframework.http.MediaType;

public class JsonHttpEntity extends HttpEntity<String> {

    public JsonHttpEntity(Json body) {
        super(body.json(), new ContentTypeHtpHeaders(MediaType.APPLICATION_JSON));
    }
}
