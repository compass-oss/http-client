package eu.compass.httpclient.domain;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

public class ContentTypeHtpHeaders extends HttpHeaders {

    public ContentTypeHtpHeaders(MediaType mediaType) {
        setContentType(mediaType);
    }
}
