package eu.compass.httpclient.domain;

import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.MediaType;

public class ApplicationOctetStreamHttpEntity extends HttpEntity<FileSystemResource> {

    public ApplicationOctetStreamHttpEntity(FileSystemResource body) {
        super(body, new ContentTypeHtpHeaders(MediaType.APPLICATION_OCTET_STREAM));
    }
}
