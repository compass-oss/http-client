package eu.compass.httpclient.request;

import eu.compass.httpclient.exception.ResponseException;
import eu.compass.httpclient.handler.UnexpectedStatusHandler;
import eu.compass.httpclient.response.Response;
import eu.compass.httpclient.handler.TimeoutUnexpectedStatusHandler;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * Builds {@link RepeatableHttpRequest} with convenient way.
 *
 * Default number of tries is one
 *
 * @param <T> Response type
 */
public class RepeatableHttpRequestBuilder<T> {

    private final URI url;
    private final Class<T> responseClass;
    private final HttpMethod httpMethod;
    private final RestTemplate restTemplate;
    private Set<HttpStatus> expectedStatus;
    private final Set<HttpStatus> unrepeatableStatusCodes;
    private HttpHeaders headers = new HttpHeaders();
    private String stringBody;
    private MultiValueMap<String, Object> multiValueBody;
    private List<UnexpectedStatusHandler> statusHandlers = new LinkedList<>();

    private int tries = 1;
    private long timeout;

    RepeatableHttpRequestBuilder(URI url, Class<T> responseClass, HttpMethod httpMethod, final RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
        this.url = url;
        this.responseClass = responseClass;
        this.httpMethod = httpMethod;
        this.unrepeatableStatusCodes = new LinkedHashSet<>();
        this.multiValueBody = new LinkedMultiValueMap<>();
        this.expectedStatus = new HashSet<>();
    }

    /**
     * Creates a new instance of HTTP POST request
     *
     * @param url Resource URL
     * @param responseClass Wanted response class
     * @param <T> Response type
     * @return Builder instantiated as POST request
     */
    public static <T> RepeatableHttpRequestBuilder<T> post(URI url, Class<T> responseClass, final RestTemplate restTemplate) {
        return new RepeatableHttpRequestBuilder<>(url, responseClass, HttpMethod.POST, restTemplate);
    }

    /**
     * Creates a new instance of HTTP GET request
     *
     * @param url Resource URL
     * @param responseClass Wanted response class
     * @param <T> Response type
     * @return Builder instantiated as GET request
     */
    public static <T> RepeatableHttpRequestBuilder<T> get(URI url, Class<T> responseClass, final RestTemplate restTemplate) {
        return new RepeatableHttpRequestBuilder<>(url, responseClass, HttpMethod.GET, restTemplate);
    }

    /**
     * Creates a new instance of HTTP PUT request
     *
     * @param url Resource URL
     * @param responseClass Wanted response class
     * @param <T> Response type
     * @return Builder instantiated as POST request
     */
    public static <T> RepeatableHttpRequestBuilder<T> put(URI url, Class<T> responseClass, final RestTemplate restTemplate) {
        return new RepeatableHttpRequestBuilder<>(url, responseClass, HttpMethod.PUT, restTemplate);
    }

    /**
     * Creates a new instance of HTTP DELETE request
     *
     * @param url Resource URL
     * @param responseClass Wanted response class
     * @param <T> Response type
     * @return Builder instantiated as DELETE request
     */
    public static <T> RepeatableHttpRequestBuilder<T> delete(URI url, Class<T> responseClass,
                                                             final RestTemplate restTemplate) {
        return new RepeatableHttpRequestBuilder<>(url, responseClass, HttpMethod.DELETE, restTemplate);
    }

    /**
     * Sets content type to "application/json"
     *
     * If there is another Content-Type value, it's overridden
     *
     * @return Builder with Content-Type header set to application/json
     */
    public RepeatableHttpRequestBuilder<T> json() {
        headers.remove("Content-Type");
        headers.add("Content-Type", MediaType.APPLICATION_JSON_VALUE);
        return this;
    }

    /**
     * Adds HTTP header
     *
     * @param header Header name
     * @param value Header value
     * @return Builder with added header
     */
    public RepeatableHttpRequestBuilder<T> header(String header, String value) {
        headers.add(header, value);
        return this;
    }


    /**
     * Appends map to built request
     *
     * If this method is called multiple times only last call is used
     *
     * @param body Body to be appended
     * @return Builder with appended body
     */
    public RepeatableHttpRequestBuilder<T> body(String body) {
        this.stringBody = body;
        return this;
    }

    /**
     * Appends pair to built request
     *
     * Set header content type to multipart form data
     *
     * @param key Name of part
     * @param value Value of part
     * @return Builder with appended body
     */
    public RepeatableHttpRequestBuilder<T> body(String key, Object value) {
        this.multiValueBody.add(key, value);
        if (!MediaType.MULTIPART_FORM_DATA.equals(headers.getContentType())) {
            headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        }
        return this;
    }

    /**
     * Sets maximum number of tries
     *
     * @param tries Maximum number of repeatable calls
     * @return Builder with maximum tries set
     */
    public RepeatableHttpRequestBuilder<T> tries(int tries) {
        this.tries = tries;
        return this;
    }

    /**
     * Sets maximum timeout for request (not for one request bot for all repeated tries)
     *
     * @param milliseconds Maximum timeout
     * @return Builder with maximum timeout set
     */
    public RepeatableHttpRequestBuilder<T> timeout(long milliseconds) {
        this.timeout = milliseconds;
        return this;
    }

    /**
     * Adds {@code httpStatus} into statuses which are not repeated.
     *
     * @param httpStatus Status code which won't be repeated
     * @return Builder with added status code
     */
    public RepeatableHttpRequestBuilder<T> doNotRepeatFor(HttpStatus httpStatus) {
        unrepeatableStatusCodes.add(httpStatus);
        return this;
    }

    /**
     * Sets a handler for unexpected status
     *
     * There can be multiple handlers for same status.
     *
     * @param handler Handler for unexpected status
     * @return Builder with added handler
     */
    public RepeatableHttpRequestBuilder<T> retry(UnexpectedStatusHandler handler) {
        statusHandlers.add(handler);
        return this;
    }

    /**
     * Adds {@code httpStatus} as an unexpected status handled by {@link TimeoutUnexpectedStatusHandler}
     *
     * @param httpStatus Unexpected status to be handled
     * @return Builder with added handler
     */
    public RepeatableHttpRequestBuilder<T> retryWithTimeout(HttpStatus httpStatus) {
        statusHandlers.add(new TimeoutUnexpectedStatusHandler(httpStatus));
        return this;
    }

    /**
     * Sends HTTP request with previously set parameters.
     *
     * Only body is extracted and returned
     *
     * @param expectedStatus Expected return status
     * @return Response converted to given type
     * @throws ResponseException If returned status is not same as expected.
     */
    public T send(HttpStatus expectedStatus) {
        return sendForCompleteResponse(expectedStatus).getResponse();
    }

    /**
     * Sends HTTP request with previously set parameters.
     *
     * Body, status and all headers are extracted and returned
     *
     * @param expectedStatus Expected return status
     * @return Response converted to given type
     * @throws ResponseException If returned status is not same as expected.
     */
    public Response<T> sendForCompleteResponse(HttpStatus expectedStatus) {
        return sendForCompleteResponse(Collections.singleton(expectedStatus));
    }

    /**
     * Sends HTTP request with previously set parameters.
     *
     * Body, status and all headers are extracted and returned
     *
     * @param expectedStatus Expected set of return status
     * @return Response converted to given type
     * @throws ResponseException If returned status is not same as one of expected.
     */
    public Response<T> sendForCompleteResponse(Set<HttpStatus> expectedStatus) {
        this.expectedStatus = expectedStatus;
        Response<T> response = new RepeatableHttpRequest<>(this, restTemplate).send();

        if (response.hasExpectedStatus()) {
            return response;
        }

        throw new ResponseException(url, response);
    }

    URI getUrl() {
        return url;
    }

    Class<T> getResponseClass() {
        return responseClass;
    }

    HttpMethod getHttpMethod() {
        return httpMethod;
    }

    Set<HttpStatus> getExpectedStatus() {
        return expectedStatus;
    }

    public Set<HttpStatus> getUnrepeatableStatusCodes() {
        return Collections.unmodifiableSet(unrepeatableStatusCodes);
    }

    HttpHeaders getHeaders() {
        return headers;
    }

    List<UnexpectedStatusHandler> getStatusHandlers() {
        return statusHandlers;
    }

    int getTries() {
        return tries;
    }

    long getTimeout() {
        return timeout;
    }

    String getStringBody() {
        return stringBody;
    }

    MultiValueMap<String, Object> getMultiValueBody() {
        return multiValueBody;
    }
}