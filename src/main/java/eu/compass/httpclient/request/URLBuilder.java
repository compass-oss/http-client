package eu.compass.httpclient.request;

import eu.compass.httpclient.exception.ResponseException;
import eu.compass.httpclient.response.Response;
import org.slf4j.Logger;
import org.springframework.http.HttpStatus;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;
import java.util.stream.Collectors;

import static org.slf4j.LoggerFactory.getLogger;

public class URLBuilder {

    private static final Logger logger = getLogger(URLBuilder.class);

    private final Map<String, Object> params;
    private final List<String> paths;
    private final String host;
    private String connType;

    /**
     * Create URL builder with Host url
     * <p>
     *      <b>If possible use Host URL with protocol already specified.</b>
     *      When Host URL does not contain Connection protocol then the Connection protocol is set to 'http'.
     *      For example:
     * </p>
     * <p>
     *      url https://127.0.0.1:8000/some/other/path/belonging/to/host/url will be https://127.0.0.1:8000/some/other/path/belonging/to/host/url
     * </p>
     * <p>
     *      url 127.0.0.1:8000/some/other/path/belonging/to/host/url will be http://127.0.0.1:8000/some/other/path/belonging/to/host/url
     * </p>
     * <p>
     *     Connection type can be changed explicitly in the builder itself trough setter
     * </p>
     * @param host URL in String format from which URLBuilder will start building URI
     */
    public URLBuilder(String host) {
        this.params = new LinkedHashMap<>();

        host = insertProtocol(host);
        URL parsedHost = parseHost(host);

        this.connType = parsedHost.getProtocol();
        this.paths = getDefaultPath(parsedHost);
        this.host = getHostString(parsedHost);
    }

    public URLBuilder setConnectionType(String conn) {
        this.connType = conn;
        return this;
    }

    public URLBuilder addSubPath(String path) {
        paths.add(getPathWithoutStartingSlash(path));
        return this;
    }

    public URLBuilder addParameter(String parameter, Object value) {
        params.put(parameter, value);
        return this;
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public URI getURL() {
        URI uri = null;
        try {
            String parameters = params.entrySet()
                    .stream()
                    .map(e -> String.format("%s=%s", e.getKey(), e.getValue() != null ? e.getValue().toString() : null))
                    .collect(Collectors.joining("&"));
            uri = new URI(connType, host, "/" + String.join("/", paths),
                    !parameters.isEmpty() ? parameters : null,
                    null);
            // To validate URI we have to call toURL without needing the actual response
            uri.toURL();
        } catch (Exception e) {
            logger.error("Build uri failed", e);
            throw new ResponseException(uri, Response.error(HttpStatus.BAD_REQUEST, "Wrong uri format"));
        }
        return uri;
    }

    private static URL parseHost(String host) {
        try {
            return new URI(host).toURL();
        } catch (URISyntaxException | MalformedURLException ex) {
            logger.debug("Host {} parsing threw exception.", host, ex);
            throw new RuntimeException(ex);
        }
    }

    private static List<String> getDefaultPath(URL parsedHost) {
        String pathWithoutStartingSlash = getPathWithoutStartingSlash(parsedHost.getPath());
        if (pathWithoutStartingSlash.isBlank()) {
            return new ArrayList<>();
        }

        return new ArrayList<>(List.of(pathWithoutStartingSlash));
    }

    private static String insertProtocol(String host) {
        if (!host.startsWith("http")) {
            host = "http://" + host;
        }
        return host;
    }

    private static String getPathWithoutStartingSlash(String path) {
        if (path.startsWith("/")) {
            path = path.substring(1);
        }
        return path;
    }

    private static String getHostString(URL parsedHost) {
        final String host;
        if (parsedHost.getPort() != -1) {
            host = parsedHost.getHost() + ":" + parsedHost.getPort();
        } else {
            host = parsedHost.getHost();
        }
        return host;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", URLBuilder.class.getSimpleName() + "[", "]")
                .add("params=" + params)
                .add("paths=" + paths)
                .add("host='" + host + "'")
                .add("connType='" + connType + "'")
                .toString();
    }
}