package eu.compass.httpclient.request;

import eu.compass.httpclient.handler.UnexpectedStatusHandler;
import eu.compass.httpclient.response.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.List;
import java.util.Set;

/**
 * Implements basic retry-timeout mechanism for HTTP requests. Springs' {@link RestTemplate} is used for actual exchange.
 *
 * Two types of error are handled.
 *
 * <ul>
 * <li>Unexpected response code</li>
 * <li>Communication error</li>
 * </ul>
 *
 * <b>Unexpected response code</b>: Each request has expected return code. If this status code does not match reality,
 * retry-timeout mechanism is triggered. Each status code can be handled differently and some of them can be marked as "do not
 * repeat". There can be multiple handlers for one state. If at least one of them is marked as "try again", request is repeated.
 *
 * <b>Communication error</b>: This error raises when targeted application is unreachable, such as no internet connection or
 * targeted application server is down. This error is typically the error that we want to retry the most.
 *
 * <b>Unrepeatable status codes</b>: Some HTTP codes can be marked as <i>unrepeatable</i>. If a status code is returned and it's
 * marked as <i>unrepeatable</i> response with an error is returned without retrying again. This is useful for <i>idempotent</i>
 * API or their part. Example: Trying to add something which is not unique always return 409, so there is no need to repeat this
 * request in such case.
 *
 * <i>Note:</i> If both application runs on same Tomcat instance and the server is redeployed, 404 is returned (Unexpected
 * response code not Communication error).
 *
 * @param <T> Response type
 */
public class RepeatableHttpRequest<T> {

    private static final Logger LOG = LoggerFactory.getLogger(RepeatableHttpRequest.class);

    private final RestTemplate restTemplate;
    private final URI url;
    private final HttpMethod httpMethod;
    private final Set<HttpStatus> expectedStatus;
    private final Set<HttpStatus> unrepeatableStatusCodes;
    private final Class<T> responseClass;
    private final HttpHeaders httpHeaders;
    private final String stringBody;
    private final MultiValueMap<String, Object> multiValueBody;
    private final List<UnexpectedStatusHandler> statusHandlers;
    private final int tries;
    private final long timeoutMs;

    RepeatableHttpRequest(RepeatableHttpRequestBuilder<T> builder, final RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
        this.url = builder.getUrl();
        this.httpMethod = builder.getHttpMethod();
        this.expectedStatus = builder.getExpectedStatus();
        this.unrepeatableStatusCodes = builder.getUnrepeatableStatusCodes();
        this.responseClass = builder.getResponseClass();
        this.httpHeaders = builder.getHeaders();
        this.stringBody = builder.getStringBody();
        this.statusHandlers = builder.getStatusHandlers();
        this.tries = builder.getTries();
        this.timeoutMs = builder.getTimeout();
        this.multiValueBody = builder.getMultiValueBody();
    }

    Response<T> send() {
        for (int i = 0; i < tries; i++) {
            try {
                Response<T> response = performHttpCall();
                if (response.hasExpectedStatus()
                    || unrepeatableStatusCodes.contains(response.getHttpStatus())
                    || !retry(response.getHttpStatus(), i + 1, "")) {
                    return response;
                }
            } catch (HttpClientErrorException httpClientEx) {
                HttpStatus httpStatus = HttpStatus.valueOf(httpClientEx.getStatusCode().value());
                if (unrepeatableStatusCodes.contains(httpStatus)) {
                    return Response.error(
                            httpStatus,
                            "Unexpected error with code marked as non-repeatable",
                            httpClientEx.getResponseBodyAsString()
                    );
                }

                if (!retry(httpStatus, i + 1, httpClientEx.getResponseBodyAsString())) {
                    return Response.error(
                            httpStatus,
                            "Unexpected client error was not valid for retrying.",
                            httpClientEx.getResponseBodyAsString()
                    );
                }
            } catch (RestClientException restClientEx) {
                if (i + 1 == tries) {
                    if (restClientEx instanceof HttpStatusCodeException statusCodeException) {
                        return Response.error(
                                HttpStatus.valueOf(statusCodeException.getStatusCode().value()),
                                "Unexpected sever error was returned on last try.",
                                statusCodeException.getResponseBodyAsString()
                        );
                    }
                }
                if (!handleRestClientException(restClientEx, i + 1)) {
                    if (restClientEx instanceof HttpStatusCodeException statusCodeException) {
                        return Response.error(
                                HttpStatus.valueOf(statusCodeException.getStatusCode().value()),
                                "Unhandled sever error was returned after failed retry.",
                                statusCodeException.getResponseBodyAsString()
                        );
                    } else {
                        return Response.error("Unhandled RestClientException");
                    }
                }
            }
        }

        return Response.error("Number of maximum tries exceeded");
    }

    public long getTimeoutForOneRepetition() {
        return timeoutMs / tries;
    }

    public void replaceHeader(String header, String newValue) {
        httpHeaders.remove(header);
        httpHeaders.add(header, newValue);
    }

    protected Response<T> performHttpCall() {
        HttpEntity<Object> request;
        if (multiValueBody.isEmpty()) {
            request = new HttpEntity<>(stringBody, httpHeaders);
        } else {
            request = new HttpEntity<>(multiValueBody, httpHeaders);
        }

        ResponseEntity<T> response = restTemplate.exchange(url, httpMethod, request, responseClass);
        HttpStatus httpStatus = getHttpStatusFromCode(response);
        if (expectedStatus.contains(httpStatus)) {
            return Response.ok(response.getBody(), httpStatus, response.getHeaders());
        }

        if (unrepeatableStatusCodes.contains(httpStatus)) {
            return Response.error(httpStatus, "Response with unrepeatable status code");
        }

        return Response.error(httpStatus, "Response with unexpected status code");
    }

    protected boolean retry(HttpStatus httpStatus, int attempt, String body) {
        boolean retry = false;
        for (UnexpectedStatusHandler statusHandler : statusHandlers) {
            if (statusHandler.getHandledStatus().equals(httpStatus)) {
                retry |= statusHandler.handle(this, body, attempt);
            }
        }

        return retry;
    }

    protected boolean handleRestClientException(RestClientException restClientEx, int attempt) {
        LOG.error("{} can not be reached in attempt #{}, sleeping for {} ms", url, attempt, timeoutMs / tries, restClientEx);

        try {
            Thread.sleep(timeoutMs / tries);
        } catch (InterruptedException ex) {
            LOG.error("Unable to wait for next attempt (Interrupted)", ex);
        }

        return true;
    }

    private HttpStatus getHttpStatusFromCode(ResponseEntity<T> response) {
        return HttpStatus.valueOf(response.getStatusCode().value());
    }

}
