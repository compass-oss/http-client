package eu.compass.httpclient;

import eu.compass.httpclient.exception.ResponseException;
import eu.compass.httpclient.request.RepeatableHttpRequest;
import eu.compass.httpclient.request.RepeatableHttpRequestBuilder;
import eu.compass.httpclient.response.Response;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.Set;

/**
 * Abstract implementation of {@link RepeatableHttpRequest} for HTTP REST call.
 */
public abstract class HttpRestClient {

    protected final RestTemplate restTemplate;

    public HttpRestClient(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    /**
     * Sends a request
     *
     * @param request Built request targeting an endpoint
     * @param expectedStatus Expected HTTP status
     * @param <T> Response type
     * @return Response mapped as T
     * @throws ResponseException If server returns anything different than {@code expectedStatus}
     */
    protected <T> T send(RepeatableHttpRequestBuilder<T> request, HttpStatus expectedStatus) {
        return sendForCompleteResponse(request, Collections.singleton(expectedStatus)).getResponse();
    }

    /**
     * Sends a request
     *
     * @param request Built request targeting an endpoint
     * @param expectedStatus Expected HTTP status
     * @param <T> Response type
     * @return Response mapped as T
     * @throws ResponseException If server returns anything different than {@code expectedStatus}
     */
    protected <T> T send(RepeatableHttpRequestBuilder<T> request, Set<HttpStatus> expectedStatus) {
        return sendForCompleteResponse(request, expectedStatus).getResponse();
    }

    /**
     * Sends a request
     *
     * @param request Built request targeting an endpoint
     * @param expectedStatus Expected HTTP status
     * @param <T> Response type
     * @return Response entity filled with body, status and all headers
     * @throws ResponseException If server returns anything different than {@code expectedStatus}
     */
    protected <T> Response<T> sendForCompleteResponse(RepeatableHttpRequestBuilder<T> request, HttpStatus expectedStatus) {
        return sendForCompleteResponse(request, Collections.singleton(expectedStatus));
    }

    /**
     * Sends a request
     *
     * @param request Built request targeting an endpoint
     * @param expectedStatus Expected HTTP status
     * @param <T> Response type
     * @return Response entity filled with body, status and all headers
     * @throws ResponseException If server returns anything different than {@code expectedStatus}
     */
    protected <T> Response<T> sendForCompleteResponse(RepeatableHttpRequestBuilder<T> request, Set<HttpStatus> expectedStatus) {
        return request.sendForCompleteResponse(expectedStatus);
    }

}
