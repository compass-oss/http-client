package eu.compass.httpclient.exception;

import eu.compass.httpclient.response.Response;
import org.springframework.http.HttpStatus;

import java.io.Serial;
import java.net.URI;

public class ResponseException extends RuntimeException {

    @Serial
    private static final long serialVersionUID = -1623076504251543567L;

    private final HttpStatus httpStatus;
    private final String nestedRawErrorResponse;

    public ResponseException(URI url, Response<?> response) {
        super(String.format("Unexpected response for %s was returned (HTTP status=%s, msg=%s)",
                url, response.getHttpStatus(), response.getErrorMessage()));
        this.httpStatus = response.getHttpStatus();
        this.nestedRawErrorResponse = response.getErrorResponseAsString();
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public String getNestedRawErrorResponse() {
        return nestedRawErrorResponse;
    }
}
