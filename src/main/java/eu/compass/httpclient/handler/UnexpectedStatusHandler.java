package eu.compass.httpclient.handler;

import eu.compass.httpclient.request.RepeatableHttpRequest;
import org.springframework.http.HttpStatus;

/**
 * Handles unexpected response by implementing {@link UnexpectedStatusHandler#handle(RepeatableHttpRequest, String, int)} method.
 *
 * Handle method can state whether request should be repeated or not.
 */
public interface UnexpectedStatusHandler {

    /**
     * Handles unexpected status
     *
     * @param request HTTP request which triggers this handler
     * @param responseBody Response returned by server with unexpected status (can be null)
     * @param attempt nth Attempt
     * @return True if request should be repeated, false otherwise
     */
    boolean handle(RepeatableHttpRequest request, String responseBody, int attempt);

    /**
     * @return HTTP status which is handled by this handler
     */
    HttpStatus getHandledStatus();
}
