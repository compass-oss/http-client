package eu.compass.httpclient.handler;

import eu.compass.httpclient.request.RepeatableHttpRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;

/**
 * Default implementation of {@link UnexpectedStatusHandler}.
 *
 * This implementation only sleeps thread for defined timeout and marks request as repeatable.
 */
public class TimeoutUnexpectedStatusHandler implements UnexpectedStatusHandler {

    private static final Logger LOG = LoggerFactory.getLogger(TimeoutUnexpectedStatusHandler.class);
    private final HttpStatus handledStatus;

    public TimeoutUnexpectedStatusHandler(HttpStatus handledStatus) {
        this.handledStatus = handledStatus;
    }

    @Override
    public boolean handle(RepeatableHttpRequest request, String responseBody, int attempt) {
        LOG.info("Handling attempt {} for status {} (Waiting {} ms for next attempt)",
                attempt, handledStatus, request.getTimeoutForOneRepetition());

        try {
            Thread.sleep(request.getTimeoutForOneRepetition());
        } catch (InterruptedException ex) {
            LOG.error("Unable to wait for next attempt (interrupted)", ex);
        }

        return true;
    }

    @Override
    public HttpStatus getHandledStatus() {
        return handledStatus;
    }
}
