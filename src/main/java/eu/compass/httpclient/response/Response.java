package eu.compass.httpclient.response;

import eu.compass.httpclient.request.RepeatableHttpRequestBuilder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;

import java.util.List;
import java.util.Optional;
import java.util.StringJoiner;

/**
 * Represents HTTP response from REST call.
 *
 * {@link Response} class can be in two states:
 * <ul>
 * <li>Filled with valid response</li>
 * <li>Filled with returned http status and error message</li>
 * </ul>
 *
 * @param <T> Class returned in HTTP response (Mapped JSON)
 */
public class Response<T> {

    private final T response;
    private final HttpStatus httpStatus;
    private final String errorMessage;
    private final String errorResponseAsString;
    private final boolean ok;
    private final HttpHeaders headers;

    private Response(T response, HttpStatus httpStatus, String errorMessage, String errorResponseAsString,
                     boolean ok, HttpHeaders httpHeaders) {
        this.response = response;
        this.httpStatus = httpStatus;
        this.errorMessage = errorMessage;
        this.errorResponseAsString = errorResponseAsString;
        this.ok = ok;
        this.headers = httpHeaders;
    }

    /**
     * Creates a new response with entity after successful request.
     *
     * @param <T> Response type
     * @param response Response returned from request
     * @param httpStatus Returned httpStatus code
     * @param headers All headers from response
     * @return Response instance with filled data
     */
    public static <T> Response<T> ok(T response, HttpStatus httpStatus, HttpHeaders headers) {
        return new Response<>(response, httpStatus, null, null, true, headers);
    }

    /**
     * Creates a response for error state
     *
     * @param httpStatus Unexpected error status
     * @param errorMessage Detailed error message
     * @param <T> Expected response type
     * @return Error response
     */
    public static <T> Response<T> error(HttpStatus httpStatus, String errorMessage) {
        return new Response<>(null, httpStatus, errorMessage, null, false, new HttpHeaders());
    }

    /**
     * Creates a response for error state
     *
     * @param httpStatus Unexpected error status
     * @param errorMessage Detailed error message
     * @param errorResponseAsString Error response from called system for further processing
     * @param <T> Expected response type
     * @return Error response
     */
    public static <T> Response<T> error(HttpStatus httpStatus, String errorMessage, String errorResponseAsString) {
        return new Response<>(null, httpStatus, errorMessage, errorResponseAsString, false, new HttpHeaders());
    }

    /**
     * Creates a response for error state
     *
     * @param errorMessage Detailed error message
     * @param <T> Expected response type
     * @return Error response
     */
    public static <T> Response<T> error(String errorMessage) {
        return new Response<>(null, null, errorMessage, null, false, new HttpHeaders());
    }

    /**
     * Tells whether the received response status is as expected.
     *
     * The expected status is set in {@link RepeatableHttpRequestBuilder#send(HttpStatus)}.
     *
     * @return True if the status is as expected. False otherwise.
     */
    public boolean hasExpectedStatus() {
        return ok;
    }

    public T getResponse() {
        return response;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public String getErrorResponseAsString() {
        return errorResponseAsString;
    }

    /**
     * Retrieves header's values from response by its name
     *
     * @param header Header's name
     * @return Optional with header's values if header is in response, empty otherwise
     */
    public Optional<List<String>> getHeaderValues(String header) {
        return Optional.ofNullable(headers.get(header));
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Response.class.getSimpleName() + "[", "]")
                .add("response=" + response)
                .add("httpStatus=" + httpStatus)
                .add("errorMessage='" + errorMessage + "'")
                .add("errorResponseAsString='" + errorResponseAsString + "'")
                .toString();
    }
}
