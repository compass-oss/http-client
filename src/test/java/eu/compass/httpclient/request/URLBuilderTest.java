package eu.compass.httpclient.request;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class URLBuilderTest {

    @Test
    void urlWithHttpPrefixHostIsCreatedWithSingleHttpPrefix() {
        assertThat(new URLBuilder("http://127.0.0.1:8000").getURL().toString()).isEqualTo("http://127.0.0.1:8000/");
    }

    @Test
    void urlWithNullParameterIsCreatedCorrectly() {
        assertThat(new URLBuilder("http://127.0.0.1:8000")
                .addSubPath("some-sub-path")
                .addParameter("someParameter", null)
                .getURL()
                .toString()
        ).isEqualTo("http://127.0.0.1:8000/some-sub-path?someParameter=null");
    }


    @Test
    void urlWithSubPathAsHostWillIsCreatedWithWithSubPath() {
        String hostUrl = "https://127.0.0.1:8000/some/other/path/belonging/to/host/url";
        String builtUrl = new URLBuilder(hostUrl).getURL().toString();

        assertThat(builtUrl).isEqualTo(hostUrl);
    }

    @Test
    void urlWithSubPathAsHostWillIsCreatedWithWithSubPaths() {
        String additionalSubPath = "/some/new/kind/of/subPath";
        String hostUrl = "https://127.0.0.1:8000/some/other/path/belonging/to/host/url";
        String builtUrl = new URLBuilder(hostUrl)
                .addSubPath(additionalSubPath)
                .getURL().toString();

        assertThat(builtUrl).isEqualTo(hostUrl + additionalSubPath);
    }

    @Test
    void urlWithoutProtocolIsCreatedCorrectly() {
        String hostUrl = "127.0.0.1";
        String builtUrl = new URLBuilder(hostUrl).getURL().toString();

        assertThat(builtUrl).isEqualTo("http://" + hostUrl + "/");
    }

    @Test
    void urlWithoutProtocolAndWithSubPathIsCreatedCorrectly() {
        String hostUrl = "127.0.0.1/some/kind/of/path";
        String builtUrl = new URLBuilder(hostUrl).getURL().toString();

        assertThat(builtUrl).isEqualTo("http://" + hostUrl);
    }
}