package eu.compass.httpclient;

import eu.compass.httpclient.exception.ResponseException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withServerError;

class HttpRestClientTest {

    private MockRestServiceServer mockServer;
    private TestHttpRestClient testHttpRestClient;

    @BeforeEach
    void setUp() {
        RestTemplate restTemplate = new RestTemplate();

        this.mockServer = MockRestServiceServer.createServer(restTemplate);
        this.testHttpRestClient = new TestHttpRestClient(restTemplate);
    }

    @Test
    @DisplayName("Client is configured with 10 sec timeout but timeout for single try should not be used")
    @Timeout(1)
    void timeoutIsNotAppliedAfterLastTry() {
        mockServer.expect(requestTo("https://mock-api.com"))
                .andExpect(method(HttpMethod.POST))
                .andRespond(withServerError().body("Server error"));

        ResponseException responseException = assertThrows(ResponseException.class, () -> testHttpRestClient.callMockAPI());

        assertThat(responseException.getHttpStatus()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
        assertThat(responseException.getMessage())
                .isEqualTo("Unexpected response for https://mock-api.com was returned" 
                        + " (HTTP status=500 INTERNAL_SERVER_ERROR, msg=Unexpected sever error was returned on last try.)");
        assertThat(responseException.getNestedRawErrorResponse())
                .isEqualTo("Server error");
        
        mockServer.verify();
    }
}