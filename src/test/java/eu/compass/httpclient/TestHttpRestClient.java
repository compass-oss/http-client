package eu.compass.httpclient;

import eu.compass.httpclient.request.RepeatableHttpRequestBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;

class TestHttpRestClient extends HttpRestClient {

    public TestHttpRestClient(RestTemplate restTemplate) {
        super(restTemplate);
    }

    public void callMockAPI() {
        send(RepeatableHttpRequestBuilder.post(createURI(), Void.class, restTemplate)
                .tries(1)
                .timeout(10_000L), 
                HttpStatus.OK)                
                ;
    }
    
    private URI createURI() {
        try {
            return new URI("https://mock-api.com");
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }
}